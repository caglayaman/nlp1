package fsttest;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

import org.junit.Test;

import fst.FstState;
import fst.MorpFst;
import fst.MyFst;

public class BasicTest {

	@Test
	public void test() {
		MyFst myFst = new MyFst("First");
		FstState startState = myFst.addState(0, true);
		FstState state1 = myFst.addState(1);
		FstState state2 = myFst.addState(2);
		FstState state3 = myFst.addState(3);
		FstState state4 = myFst.addState(4);
		FstState state5 = myFst.addState(5);
		FstState state6 = myFst.addState(6, true);
		myFst.addState(7);

		myFst.addTransition("x", "x", 0, 1);
		myFst.addTransition("z", "z", 0, 1);
		myFst.addTransition("c", "c", 0, 2);
		myFst.addTransition("s", "s", 0, 3);
		myFst.addTransition("@", "@", 0, 0);

		myFst.addTransition("x", "x", 1, 1);
		myFst.addTransition("z", "z", 1, 1);
		myFst.addTransition("s", "s", 1, 3);
		myFst.addTransition("c", "c", 1, 2);
		myFst.addTransition("e", "�", 1, 5);
		myFst.addTransition("@", "@", 1, 0);

		myFst.addTransition("h", "h", 2, 4);
		myFst.addTransition("x", "x", 2, 1);
		myFst.addTransition("z", "z", 2, 1);
		myFst.addTransition("s", "s", 2, 3);
		myFst.addTransition("e", "�", 2, 5);
		myFst.addTransition("@", "@", 2, 0);

		myFst.addTransition("h", "h", 3, 4);
		myFst.addTransition("s", "s", 3, 3);
		myFst.addTransition("x", "x", 3, 1);
		myFst.addTransition("z", "z", 3, 1);
		myFst.addTransition("e", "�", 3, 5);
		myFst.addTransition("@", "@", 3, 0);

		myFst.addTransition("e", "�", 4, 5);
		myFst.addTransition("@", "@", 4, 0);

		myFst.addTransition("s", "^s", 5, 6);
		myFst.addTransition("@", "e", 5, 0);

		myFst.start = startState;
		Stack<String> inputStack = new Stack<>();
		inputStack.push("s");
		inputStack.push("e");
		inputStack.push("x");
		inputStack.push("o");
		inputStack.push("f");

		Stack<String> output = myFst.run(inputStack);

		while (!output.isEmpty()) {
			System.out.print(output.pop());
		}

	}

	@Test
	public void testCombine() {
		MyFst surfTagFst = MyFst.create("file/surfaceTag.fst");
		MyFst surfToInt = MyFst.create("file/surfaceToInt.fst");
		surfToInt.combine(surfTagFst);
		System.out.print("foxes --> ");
		Stack<String> output = surfToInt.run("foxes");
		while (!output.isEmpty()) {
			System.out.print(output.pop());
		}
		
		System.out.println();
		System.out.print("window --> ");
		output = surfToInt.run("window");
		List<String> collect = output.stream().collect(Collectors.toList());
		while (!output.isEmpty()) {
			System.out.print(output.pop());
		}
		
		System.out.println();
		System.out.print("teaches --> ");
		output = surfToInt.run("teaches");
		while (!output.isEmpty()) {
			System.out.print(output.pop());
		}
	}
	
	@Test
	public void testFull() {
		MyFst surfTagFst = MyFst.create("file/surfaceTag.fst");
		MyFst surfToInt = MyFst.create("file/surfaceToInt.fst");
		MyFst intToMorph = MorpFst.create("file/intToMorph.fst");
		surfToInt.combine(surfTagFst);
		surfToInt.nextFst = intToMorph;
		System.out.print("foxes --> ");
		Stack<String> output = surfToInt.run("foxes");
		while (!output.isEmpty()) {
			System.out.print(output.pop());
		}
		
		System.out.println();
		System.out.print("window --> ");
		output = surfToInt.run("window");
		while (!output.isEmpty()) {
			System.out.print(output.pop());
		}
		
		System.out.println();
		System.out.print("teaches --> ");
		output = surfToInt.run("teaches");
		while (!output.isEmpty()) {
			System.out.print(output.pop());
		}
	}
}
