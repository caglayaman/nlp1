package fst;

import java.util.Stack;

public class MorpFst extends MyFst {

	public MorpFst(String name) {
		super(name);
	}
	
	@Override
	public Stack<String> run(String inputText) {
		Stack<String> newStack = new Stack<>();
		String[] inputArr = inputText.split("^");
		for (int i = inputArr.length - 1; i >= 0; i--) {
			newStack.push(String.valueOf(inputArr[i]));
		}
		return run(newStack);
	}
	
	public static MyFst create(String fileName) {
		MorpFst fst = new MorpFst(fileName);
		return init(fileName, fst);
	}
}
