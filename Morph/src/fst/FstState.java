package fst;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Stack;
import java.util.stream.Collectors;

public class FstState {
	public MyFst myFst;
	public int id;
	public boolean isFinal;
	public List<Transition> transitions = new ArrayList<>();

	public boolean trace() {
		if (myFst.inputStack != null) {
			// If input stack is empty, there is only a chance of changing
			if (myFst.inputStack.isEmpty()) {
				if (this.isFinal) {
					return true;
				}
			} else {
				String inputStr = myFst.inputStack.pop();
				List<Transition> vTransitions = transitions.stream().filter(t -> !t.input.equals("@"))
						.collect(Collectors.toList());
				if (vTransitions != null) {
					for (Transition transition : vTransitions) {
						if (executeTransition(inputStr, transition)) {
							return true;
						}
					}
				}
				Optional<Transition> otherMatch = transitions.stream().filter(t -> t.input.equals("@")).findFirst();
				if (otherMatch.isPresent()) {
					if (executeTransition(inputStr, otherMatch.get())) {
						return true;
					}
				}
				myFst.inputStack.push(inputStr);
				return false;
			}
		}

		return false;
	}

	private boolean executeTransition(String inputStr, Transition transition) {
		// if input is empty then push back the popped item to the stack (do not move
		// the cursor)
		if (transition.input.equals("£")) {
			myFst.inputStack.push(inputStr);
		}
		// If input is matched there is a chance that this can be the right path
		if (matches(transition.input, inputStr) || transition.input.equals("@")) {
			// If this state is final and the cursor is at the end of the input then this is
			// a success state. If the state is not final and the next state returns success
			// then this state is on the successful path
			if (transition.next.trace()) {
				putOutput(inputStr, transition);
				return true;
			}
		}
		return false;
	}

	private void putOutput(String inputStr, Transition transition) {
		if (transition.output.equals("£")) {// If the output is empty, then do not push anything to the output stack
			return;
		} else if (transition.output.equals("@")) {// If the output is the same as input
			myFst.outputStack.push(inputStr);
		} else {
			myFst.outputStack.push(transition.output);
		}
	}

	private boolean matches(String input, String inputStr) {
		return input.equals(inputStr) || input.equals("@");
	}

	public FstState(MyFst myFst, int id, boolean isFinal) {
		this.myFst = myFst;
		this.id = id;
		this.isFinal = isFinal;
	}

	public Transition addTransition(String input, String output, FstState next) {
		Transition transition = new Transition(input, output, next);
		this.transitions.add(transition);
		return transition;
	}

	public void addTransition(String input, String output, int next) {
		addTransition(input, output, this.myFst.states.get(next));
	}

	class Transition {
		public Transition(String input, String output, FstState next) {
			this.input = input;
			this.output = output;
			this.next = next;
		}

		public String input;
		public String output;
		public FstState next;
	}

}
