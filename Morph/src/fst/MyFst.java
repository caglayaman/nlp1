package fst;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.management.RuntimeErrorException;

import fst.FstState.Transition;

public class MyFst {
	public String name;
	public Map<Integer, FstState> states = new HashMap();
	public FstState start;
	public MyFst nextFst;

	Stack<String> inputStack;
	Stack<String> outputStack;

	public Stack<String> run(String inputText) {
		Stack<String> newStack = new Stack<>();
		char[] charArray = inputText.toCharArray();
		for (int i = charArray.length - 1; i >= 0; i--) {
			newStack.push(String.valueOf(charArray[i]));
		}
		return run(newStack);
	}

	public Stack<String> run(Stack<String> inputStack) {
		this.inputStack = inputStack;
		outputStack = new Stack<>();
		if (inputStack != null && !inputStack.isEmpty()) {
			this.start.trace();
		}
		if (this.outputStack != null && !this.outputStack.isEmpty()) {
			if (this.nextFst != null) {
				String nextInput = "";
				for (int i = this.outputStack.size() - 1; i >= 0; i--) {
					String strOut = this.outputStack.get(i);
					if (!"£".equals(strOut)) {
						nextInput = nextInput.concat(strOut);
					}
				}
				Stack<String> nextResult = this.nextFst.run(nextInput);
				nextResult.push("/");
				nextResult.push(nextInput);
				return nextResult;
			}
		}
		return outputStack;
	}

	public FstState addState(int id, boolean isfinal) {
		FstState newState = addState(id);
		newState.isFinal = isfinal;
		return newState;
	}

	public void addTransition(String input, String output, int from, int to) {
		this.states.get(from).addTransition(input, output, this.states.get(to));
	}

	public FstState addState(int id) {
		if (states.containsKey(id)) {
			throw new RuntimeException("State " + id + " already exists in " + this.name);
		}
		FstState newState = new FstState(this, id, false);
		this.states.put(id, newState);
		return newState;
	}

	public MyFst(String name) {
		super();
		this.name = name;
	}

	public static MyFst create(String fileName) {
		MyFst myFst = new MyFst(fileName);
		return init(fileName, myFst);
	}

	protected static MyFst init(String fileName, MyFst myFst) {
		try {
			File file = new File(fileName);
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String line;
			boolean statesStarted = false;
			while ((line = bufferedReader.readLine()) != null) {
				if (line.equals(""))
					continue;
				String[] split = line.split(" ");

				if (!statesStarted) {
					if ("---".equals(line)) {
						statesStarted = true;
					} else {
						int stNo = Integer.parseInt(split[0]);
						FstState fstState = myFst.states.get(stNo);
						if (fstState == null) {
							myFst.addState(stNo, true);
						}
					}
				} else {
					int stNo = Integer.parseInt(split[0]);
					int nexSt = Integer.parseInt(split[1]);
					String input = split[2];
					String output = split[3];
					FstState findFirst = myFst.states.get(stNo);
					FstState findNext = myFst.states.get(nexSt);

					FstState state;
					if (findFirst == null) {
						state = myFst.addState(stNo, true);
					} else {
						state = findFirst;
					}
					if (findNext == null) {
						myFst.addState(nexSt, false);
					}
					state.addTransition(input, output, nexSt);
				}
			}
			fileReader.close();
			FstState startState = myFst.states.get(0);
			if (startState != null) {
				myFst.start = startState;
			}
			myFst.states.values().stream().forEach(
					s -> s.isFinal = (s.transitions.stream().noneMatch(tr -> tr.next != s)) ? true : s.isFinal);
			return myFst;
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	/**
	 * Combines this FST (outer) with the given FST(inner) by replacing composite
	 * inputs and outputs with the state transitions of the second FST It replaces
	 * an input or output if the format is like '[.....]'
	 * 
	 * @param fst
	 * @return combined FST
	 */
	public MyFst combine(MyFst fst) {
		// copy FST for the loop
		List<FstState> currentStates = new ArrayList<>();
		currentStates.addAll(this.states.values());
		for (FstState state : currentStates) {
			List<Transition> stateTrans = new ArrayList<>();
			stateTrans.addAll(state.transitions);
			for (Transition transition : stateTrans) {
				// If the input is composite
				if (transition.input.contains("[")) {
					// Find the states with id 0 that takes the same input
					final List<Transition> innerTransList = new ArrayList<>();
					FstState fstState = fst.states.get(0);
					List<Transition> collect = fstState.transitions.stream()
							.filter(tr -> tr.input.equals(transition.input)).collect(Collectors.toList());
					if (collect != null) {
						innerTransList.addAll(collect);
					}
					if (!innerTransList.isEmpty()) {
						// Last state of inner states will be atttached to the next state of the outer
						// FST
						FstState finalState = transition.next;
						if (!transition.output.equals(transition.input)) {
							final String finalOutput = transition.output;
							FstState deleteState = this.addState(this.states.size());
							deleteState.addTransition("£", finalOutput, finalState);
							Transition prevTran = transition;
							for (Transition innertrans : innerTransList) {
								FstState newState = this.addState(this.states.size());
								prevTran.next = newState;
								prevTran = newState.addTransition(innertrans.output, "£", deleteState);
							}
							if (finalOutput.contains("[")) {
								final List<Transition> outerTransList = new ArrayList<>();
								FstState fstOutputState = fst.states.get(0);
								Stream<Transition> filter = fstOutputState.transitions.stream()
										.filter(tr -> tr.input.equals(finalOutput));
								if (filter != null) {
									outerTransList.addAll(filter.collect(Collectors.toList()));
								}
								if (!outerTransList.isEmpty()) {
									if (!transition.output.equals(transition.input)) {
										prevTran = deleteState.transitions.get(0);
										for (Transition innertrans : outerTransList) {
											FstState newState = this.addState(this.states.size());
											prevTran.next = newState;
											prevTran = newState.addTransition("£", innertrans.output, finalState);
										}
									}
								}
							}
						} else {
							state.transitions.remove(transition);
							manageInnerTransitions(state, innerTransList, finalState, fstState);
						}
					}
				}
			}
		}
		return this;
	}

	private void manageInnerTransitions(FstState state, final List<Transition> innerTransList, FstState finalState,
			FstState fstState) {
		for (Transition innertrans : innerTransList) {
			if (innertrans.next != null) {
				if (innertrans.next.transitions != null && !innertrans.next.transitions.isEmpty()) {
					if (!innertrans.next.equals(fstState)) {
						FstState newState = this.addState(this.states.keySet().size());
						state.addTransition(innertrans.output, innertrans.output, newState);
						manageInnerTransitions(newState, innertrans.next.transitions, finalState, innertrans.next);
					} else {
						state.addTransition(innertrans.output, innertrans.output, state);
					}
				} else {
					state.addTransition(innertrans.output, innertrans.output, finalState);
				}
			}
		}
	}
}
